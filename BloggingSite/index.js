// Adding required libraries
const express = require('express');
const path = require('path');
const mongoose = require("mongoose");
const session = require('express-session');
const {check, validationResult, body} = require('express-validator');
const fileUpload = require('express-fileupload');

// Mongo DB connection
mongoose.connect('mongodb://localhost:27017/bloggingSite',{
    useNewUrlParser: true,
    useUnifiedTopology: true
})

// Login DB model
const Admin = mongoose.model('Admin', {
    username: String,
    password: String
});
// Blog data DB model
const Blog = mongoose.model('Blog', {
    topic: String,
    content: String,
    image: String
});
// Express declarations
var app = express();
app.use(express.urlencoded({extended:false}));
app.set('views', path.join(__dirname, 'views'));
app.use(express.static(__dirname+'/public'));
app.set('view engine', 'ejs');
app.use(fileUpload());
app.use(session({
    secret: 'thisisaverysecretcode',
    resave: false,
    saveUninitialized: true
}));
// routing for home page 
app.get('/',function(req, res){
    Blog.find({}).exec(function(error, blogs){
        res.render('main', {blogs:blogs, welcome: ""});
    });   
});
// Routing for blog pages
app.get('/blog/:blogId',function(req, res){
    var blogId = req.params.blogId;
    Blog.find({}).exec(function(error, blogs){
        if(blogs){
            res.render('blog', {blogs:blogs, blogId:blogId});
        }
        else{
            res.render('blog', {message: "Page does not exist"});
        }
    });
});
// Routing to display login page
app.get('/login', function(req, res){
    if (!req.session.userLoggedIn){
        res.render('login');
    }
    else {
        res.redirect('/dashboard');
    }
});
// Routing for validating login
app.post('/login', function(req, res){
    var user = req.body.username;
    var pass = req.body.password;
    Admin.findOne({username: user, password: pass}).exec(function(err, admin){
        if(admin){
            req.session.username = admin.username;
            req.session.userLoggedIn = true;
            res.render('status', {message: 'Welcome Admin !!', welcome: "Welcome Admin"});
        }
        else{
            res.render('login', {error: 'Login denied. Invalid Credentials !!'});
        }
    });
});
// Routing for logout
app.get('/logout',function(req, res){
    req.session.userLoggedIn = false;
    res.render('login', {error : "Successfully logged out"});
});
// Routing for dashboard access
app.get('/dashboard',function(req, res){
    if (req.session.userLoggedIn){
    Blog.find({}).exec(function(error, blogs){
        res.render('dashboard', {blogs:blogs});
        })
    }
    else {
        res.render('status',{message: "Invalid access", welcome: ""});
    }
});
// Routing to display edit blog page
app.get('/edit/:blogId',function(req, res){
    var blogId = req.params.blogId;
    if (req.session.userLoggedIn){
    Blog.findOne({ _id:blogId }).exec(function(error, blog){
        if(blog){
            res.render('edit', {blog:blog});
        }
        else{
            res.render('edit', {message: "Page does not exist", welcome: "Welcome Admin"});
        }
    });
    }
    else {
        res.render('status',{message: "Invalid access", welcome: ""});
    }
});
// Routing to update edited blog data in DB 
app.post('/edit/:blogId',[
    check('topic', 'Please enter a topic name.').not().isEmpty(),
	check('content', 'Content cannot be empty.').not().isEmpty()
], function(req,res){
    if (req.session.userLoggedIn){
        const errors = validationResult(req);
        if(!errors.isEmpty())
        {
            res.render('status',{message: "Entered details are incorrect", welcome: "Welcome Admin"});
        }
        else
        {
            var topic = req.body.topic;
            var content = req.body.content;
            var image = req.files.image.name;
        
            var imageFile = req.files.image;
            var imagePath = 'public/uploads/' + image;
            imageFile.mv(imagePath, function(error){
            });
            blogId = req.params.blogId;
            Blog.findOne({_id: blogId}).exec(function(error, blog){
	    		blog.topic = topic;
	    		blog.content = content;
	    		blog.image = image;
	    		blog.save();
                res.render('status', {message: "Page updated successfully", welcome: "Welcome Admin"});
            });
        };
    }
});
// Routing for delete blog pages
app.get('/delete/:blogId',function(req, res){
    if (req.session.userLoggedIn){
    var blogId = req.params.blogId;
    Blog.findByIdAndDelete({_id: blogId}).exec(function(error, blog){
        if(blog){
            res.render('status', {message: "Page deleted successfully", welcome: "Welcome Admin"});
        }
        else{
            res.render('status', {message: "Sorry, could not delete", welcome: "Welcome Admin"});
        }
    });
    }
    else {
        res.render('status',{message: "Invalid access", welcome: ""});
    }
});
// Routing for add page 
app.get('/add',function(req, res){
    if (req.session.userLoggedIn){
        res.render('add');
    }
    else {
        res.render('status',{message: "Invalid access", welcome: ""});
    }
});
// Routing for saving a new page in DB
app.post('/process',[
    check('topic', 'Please enter a topic name.').not().isEmpty(),
	check('content', 'Content cannot be empty.').not().isEmpty()
], function(req,res){
    const errors = validationResult(req);
    if(!errors.isEmpty())
    {
        res.render('status',{message: "Entered details are incorrect", welcome: "Welcome Admin"});
    }
    else
    {
        var topic = req.body.topic;
        var content = req.body.content;
        var image = req.files.image.name;

        var imageFile = req.files.image;
        var imagePath = 'public/uploads/' + image;
        imageFile.mv(imagePath, function(error){
        });
        var pageData = {
            topic : topic,
            content : content,
            image: image
        }
        var newBlog = new Blog(pageData);
        newBlog.save();
        res.render('status', {message: "Page added successfully", welcome: "Welcome Admin"});
    }
  });
//  Listening port
app.listen(8080);

console.log('Everything executed fine.. website at port 8080....');